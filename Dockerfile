FROM bitnami/python:3.9

RUN pip install poetry

COPY app/pyproject.toml /app
COPY app/poetry.lock /app

WORKDIR /app
RUN poetry install
