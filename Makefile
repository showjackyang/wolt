run:
	docker-compose up -d

check:
	cd app && poetry install && poetry run flake8 ./ && poetry run mypy ./

unit-test:
	cd app && poetry run pytest

test: check unit-test
	echo "Test Done"

e2e-test: run
	docker-compose run opening-hours poetry run pytest

docker-build:
	docker-compose build opening-hours
