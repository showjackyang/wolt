# Opening Hours

## Prerequisites

- [docker](https://www.docker.com/)
- [docker-compose](https://docs.docker.com/compose/)
- [GNU Make](https://www.gnu.org/software/make/)

## Start up
``` sh
> make run
```
then go to [open api page](http://localhost:8000/docs) on locahost port 8000.

## Run test

Apart from base cases, edge cases are also covered via [unit tests](https://gitlab.com/showjackyang/wolt/-/blob/master/app/tests/test_libs/test_hours_parser.py), for example

* If input timestamps are not sorted.
* If timestamp is not an exact hour.
* If opening hour spans multiple days.
* If missing opening or closing time.

... etc

Install [poetry](https://python-poetry.org/) using python3.9+

``` sh
> make test
```

## Run test in container 

```sh
> make e2e-test
```

## If need to rebuild the docker image after updating dependencies

``` sh
> make docker-build
```
# Thoughts on input format

1. Use "hour of day" instead of "sec of day" as timestamp, since this will be the granularity of the final output. This also prevents "sec of day" values that cannot be matched to certain hour of day, e.g. 36001 sec cannot confusing since it's between 10AM and 11AM.
2. Flatten the nested structure, "weekday" and "timestamp of day" can be at same level as tuples, for example: [('monday', '8AM', 'open'), ('monday', '9PM', 'close'), ...]. This helps with cross-day processing, cross-week processing, and sorting.
3. Or better yet, use timestamp-tuple pairs as opening hours representation already, for example: [(('monday', '8AM', 'open'), ('monday', '9PM', 'close')), (('tuesday', '8AM', 'open'), ('tuesday', '9PM', 'close')), ...]

# Design process

## Extensibility and readability of the code

Following SOLID priciple, dependent classes are injected into usage methods with interface types guarding its compatibility. For instance, [the post request handler](https://gitlab.com/showjackyang/wolt/-/blob/master/app/api/v1/hours.py) utilizes [fastapi's denpendency injection util](https://fastapi.tiangolo.com/advanced/testing-dependencies/), thus enables flexibility for replacing depedencies while testing or extension.

[Static type check and linting](https://gitlab.com/showjackyang/wolt/-/blob/master/Makefile#L4) are also used to ensure readability in both aesthetic and interface correctness.

## How the files and folders are organized in the filesystem

Root level folder contains only files necessary to start and understand the application. The core functionalities are all inside [app folder](https://gitlab.com/showjackyang/wolt/-/tree/master/app) like:

```text
.
├── api
│   ├── deps.py
│   └── v1
│       ├── __init__.py
│       ├── api_router.py
│       └── hours.py
├── crud
│   ├── __init__.py
│   └── opening_hours.py
├── libs
│   ├── __init__.py
│   └── hours_parser.py
├── main.py
├── schema
│   └── opening_hours.py
├── services
│   ├── __init__.py
│   └── convert.py
└── tests
    ├── __init__.py
    ├── test_e2e.py
    └── test_libs
        ├── __init__.py
        └── test_hours_parser.py
```

Following SOLID priciple, all business resposibilities are seperated into different python files and modules. All imports or references are implemented in a way that's easy to replace or add, but not changing (open-close principle). For instance, 
1. [core business logic is extracted away from router](https://gitlab.com/showjackyang/wolt/-/blob/master/app/api/v1/hours.py#L24) to allowing easy replacement.
2. Also dependent services are also [injected from service modules to avoid tangling with business logic](https://gitlab.com/showjackyang/wolt/-/blob/master/app/crud/opening_hours.py#L14).

Also this is a perfectly [12-factor-ed](https://12factor.net/) app.

## Resilience of the service errors

* [Basic logging config](https://gitlab.com/showjackyang/wolt/-/blob/master/app/logging.conf)
* [custom error types](https://gitlab.com/showjackyang/wolt/-/blob/master/app/services/convert.py#L28)
* [data schema validations](https://gitlab.com/showjackyang/wolt/-/blob/master/app/schema/opening_hours.py)

are included to help catch issues during runtime.

## Testing suite

Easy [intergrated E2E tests, unit test, and staic check](https://gitlab.com/showjackyang/wolt/-/blob/master/Makefile#L10) are included.

## Use of asyncio library and asynchronous patterns

Aysnc pattern is used, thanks to [Fastapi](https://fastapi.tiangolo.com/).
