#!/usr/bin/env python3

from fastapi import APIRouter

from .hours import router as hours_router

api_router = APIRouter()
api_router.include_router(
    hours_router,
    prefix='/convert_hours',
    tags=['hours_endpoint']
)
