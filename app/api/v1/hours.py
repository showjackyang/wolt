#!/usr/bin/env python3
from typing import Any
import logging

from fastapi import APIRouter, HTTPException, Depends

from api import deps
from crud import opening_hours
from services import ConvertError
from schema.opening_hours import HoursIn, HoursResponse


logger = logging.getLogger('simple')
router = APIRouter()


@router.post('/', response_model=HoursResponse)
async def convert_hours_to_text(
        *,
        hours_in: HoursIn,
        convert_service=Depends(deps.get_convert_service),
) -> Any:
    try:
        return await opening_hours.convert(
            hours_in,
            convert_service)
    except ConvertError as ex:
        raise HTTPException(status_code=422, detail=str(ex))
    except Exception:
        logging.error('Oops, sth unexpected happened.')
        raise HTTPException(status_code=500)
