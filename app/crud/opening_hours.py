#!/usr/bin/env python3
import logging

from schema.opening_hours import HoursIn, HoursResponse
from services.convert import ConvertService
from services import ConvertError


logger = logging.getLogger('simple')


async def convert(
        hours_in: HoursIn,
        convert_service: ConvertService,
) -> HoursResponse:
    # where to add (open-close principle)
    # for future crud, i.e. DB operations
    try:
        return HoursResponse(
            opening_hours=convert_service.convert(hours_in))
    except ConvertError as ex:
        logging.warning(f'Oops, conversion error: {str(ex)}')
        raise ex
