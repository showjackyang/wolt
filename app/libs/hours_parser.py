from schema.opening_hours import Weekday


class MissOpeningHourError(Exception):
    ...


class MissClosingHourError(Exception):
    ...


class NotFullHourError(Exception):
    ...


class NextDayError(Exception):
    ...


def get_next_day(today: str) -> str:
    return {
        'Monday': 'Tuesday',
        'Tuesday': 'Wednesday',
        'Wednesday': 'Thursday',
        'Thursday': 'Friday',
        'Friday': 'Saturday',
        'Saturday': 'Sunday',
        'Sunday': 'Monday',
    }[today]


def to_am_pm_of_day(day_pin: int) -> str:
    hour, rem = divmod(day_pin, 60 * 60)

    if hour >= 24:
        raise NextDayError

    if rem:
        raise NotFullHourError

    am_pm, of_day = '', ''
    if hour < 12:
        am_pm, of_day = 'AM', str(hour)
    else:
        am_pm, of_day = 'PM', str(hour - 12)

    return f'{of_day} {am_pm}'


def convert_to_time_pins(json_) -> list[tuple[str, str, str]]:
    pins = {}

    for day in Weekday:
        base = day.value * 60 * 60 * 24

        for event_item in json_.get(day.name, []):
            secs, event = event_item['value'], event_item['type']

            pins[base + secs] = (
                event,
                to_am_pm_of_day(secs),
                day.name[0].upper() + day.name[1:]
            )

    return [pins[key] for key in sorted(pins.keys())]


def check_open_close(
        pins: list[tuple[str, str, str]]) -> None:

    def raise_error(event: str):
        if event == 'close':
            raise MissOpeningHourError
        else:
            raise MissClosingHourError

    # check alternate
    current = None
    for event, _, _ in pins:
        if event == current:
            raise_error(event)
        else:
            current = event

    # check pairs
    if len(pins) % 2 != 0:
        raise_error(event)


def handle_cross_week(
        pins: list[tuple[str, str, str]]) -> list[tuple[str, str, str]]:
    first_event, _, _ = pins[0]
    if first_event == 'close':
        pins = [pins[-1]] + pins[:-1]
    return pins


def format_ret(ret: list[str]) -> str:
    d_ = {}
    for day in ret:
        wk, tm = day.split(':')
        d_[wk] = tm.strip()

    ret = []
    for day in [
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            'Friday',
            'Saturday',
            'Sunday',
    ]:
        try:
            ret.append('{}: {}'.format(day, d_[day]))
        except KeyError:
            ret.append('{}: {}'.format(day, 'Closed'))

    return '\n'.join(ret)


def json_to_hours(json_) -> str:
    pins = convert_to_time_pins(json_)
    check_open_close(pins)
    pins = handle_cross_week(pins)

    ret: list[str] = []
    current_day = ''
    hours = ''

    def end_day(ret, hours):
        hours = hours[:-2]  # remove last ", "
        if hours.strip():
            ret.append(hours)
        return ret

    def end_multiple_days(ret, hours, hour_am_pm, current_day, day):
        hours += ' 12 PM'
        ret.append(hours)
        next_day = get_next_day(current_day)
        while next_day != day:
            ret.append(f'{next_day}: 0 AM - 12 PM')
            next_day = get_next_day(next_day)
        ret.append(f'{day}: 0 AM - {hour_am_pm}')
        return ret

    for pin_event in pins:
        event, hour_am_pm, day = pin_event

        if not current_day:
            current_day, hours = day, f'{day}: '  # mark new day

        if day != current_day:
            if event == 'open':
                ret = end_day(ret, hours)
                current_day, hours = day, f'{day}: '  # mark new day
            elif day != get_next_day(current_day):
                ret = end_multiple_days(
                    ret, hours, hour_am_pm, current_day, day)
                current_day, hours, hour_am_pm = '', '', ''

        hours += f'{hour_am_pm} -' \
            if event == 'open' else f' {hour_am_pm}, '

    ret = end_day(ret, hours)
    return format_ret(ret)
