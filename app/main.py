#!/usr/bin/env python3
import logging.config

from fastapi import FastAPI
import uvicorn
from starlette.middleware.cors import CORSMiddleware

from api.v1 import api_router


logging.config.fileConfig('logging.conf')
app = FastAPI(
    title="Opening Hours API",
    description="For opening hours.",
    version="0.0.1",
)


app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_credentials=True,
    allow_methods=['*'],
    allow_headers=['*'],
)


@app.get('/ping')
async def ping():
    return 'pong'


app.include_router(api_router)


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8080)
