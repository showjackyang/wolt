#!/usr/bin/env python3
from enum import Enum
from typing import Optional, List

from pydantic import BaseModel, validator


class OpeningHour(BaseModel):
    type: str
    value: int

    @validator('type')
    def type_must_be_open_or_close(cls, v_):
        if v_ not in {'open', 'close'}:
            raise ValueError('type is either "open" or "close"')
        return v_.title()

    @validator('value')
    def value_must_between(cls, v_):
        if not 0 <= int(v_) < 86400:
            raise ValueError('values are between 0 <= value < 86400')
        return v_


class HoursIn(BaseModel):
    monday: Optional[List[OpeningHour]] = None
    tuesday: Optional[List[OpeningHour]] = None
    wednesday: Optional[List[OpeningHour]] = None
    thursday: Optional[List[OpeningHour]] = None
    friday: Optional[List[OpeningHour]] = None
    saturday: Optional[List[OpeningHour]] = None
    sunday: Optional[List[OpeningHour]] = None

    class Config:
        schema_extra = {
            "example": {
                'monday': [
                    {
                        'type': 'open',
                        'value': 32400
                    },
                    {
                        'type': 'close',
                        'value': 72000
                    }
                ],
                'tuesday': [
                    {
                        'type': 'open',
                        'value': 32400
                    },
                    {
                        'type': 'close',
                        'value': 72000
                    }
                ],
                'wednesday': [
                    {
                        'type': 'open',
                        'value': 32400
                    },
                    {
                        'type': 'close',
                        'value': 72000
                    }
                ],
                'thursday': [
                    {
                        'type': 'open',
                        'value': 32400
                    },
                    {
                        'type': 'close',
                        'value': 72000
                    }
                ],
                'friday': [
                    {
                        'type': 'open',
                        'value': 32400
                    },
                    {
                        'type': 'close',
                        'value': 72000
                    }
                ],
                'saturday': [
                    {
                        'type': 'open',
                        'value': 32400
                    },
                    {
                        'type': 'close',
                        'value': 72000
                    }
                ],
                'sunday': [
                    {
                        'type': 'open',
                        'value': 32400
                    },
                    {
                        'type': 'close',
                        'value': 72000
                    }
                ],
            }
        }


class HoursResponse(BaseModel):
    opening_hours: str


class Weekday(Enum):
    monday = 0
    tuesday = 1
    wednesday = 2
    thursday = 3
    friday = 4
    saturday = 5
    sunday = 6
