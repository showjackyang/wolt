#!/usr/bin/env python3
from schema.opening_hours import HoursIn
from . import ConvertError
from libs.hours_parser import (
    json_to_hours,
    MissOpeningHourError,
    MissClosingHourError,
    NotFullHourError,
    NextDayError,
)


def lowercase_text(v_):
    ret = []
    for pin in v_:
        pin['type'] = pin['type'].lower()
        ret.append(pin)
    return ret


class ConvertService:
    @staticmethod
    def convert(hours_in: HoursIn) -> str:
        json_ = {}
        for k_, v_, in hours_in.dict().items():
            if v_ is not None:
                json_[k_] = lowercase_text(v_)
        try:
            return json_to_hours(json_)
        except MissOpeningHourError:
            raise ConvertError('Missing Opening Hour.')
        except MissClosingHourError:
            raise ConvertError('Missing Closing Hour.')
        except NotFullHourError:
            raise ConvertError('Not Full Hour.')
        except NextDayError:
            raise ConvertError('Value exceeds one day range, 86400.')
        except Exception:
            raise ConvertError
