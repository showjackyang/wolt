#!/usr/bin/env python3

from fastapi.testclient import TestClient
# import pytest

from main import app

client = TestClient(app)


def test_ping():
    response = client.get("/ping")
    assert response.status_code == 200


def test_convert_hours():
    response = client.post("/convert_hours/", json={
        'monday': [
            {
                'type': 'open',
                'value': 32400
            },
            {
               'type': 'close',
               'value': 72000
            }
        ],
        'tuesday': [
            {
                'type': 'open',
                'value': 32400
            },
            {
                'type': 'close',
                'value': 72000
            }
        ],
        'wednesday': [
            {
                'type': 'open',
                'value': 32400
            },
            {
                'type': 'close',
                'value': 72000
            }
        ],
        'thursday': [
            {
                'type': 'open',
                'value': 32400
            },
            {
                'type': 'close',
                'value': 72000
            }
        ],
        'friday': [
            {
                'type': 'open',
                'value': 32400
            },
            {
                'type': 'close',
                'value': 72000
            }
        ],
        'saturday': [
            {
                'type': 'open',
                'value': 32400
            },
            {
                'type': 'close',
                'value': 72000
            }
        ],
        'sunday': [
            {
                'type': 'open',
                'value': 32400
            },
            {
                'type': 'close',
                'value': 72000
            }
        ],
    })
    assert response.status_code == 200
    assert response.json() == {
        'opening_hours': 'Monday: 9 AM - 8 PM\n'
        'Tuesday: 9 AM - 8 PM\n'
        'Wednesday: 9 AM - 8 PM\n'
        'Thursday: 9 AM - 8 PM\n'
        'Friday: 9 AM - 8 PM\n'
        'Saturday: 9 AM - 8 PM\n'
        'Sunday: 9 AM - 8 PM'}
