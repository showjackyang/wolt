import pytest

from libs.hours_parser import (
    json_to_hours,
    MissOpeningHourError,
    MissClosingHourError,
    NotFullHourError,
    NextDayError,
)


def test_json_to_hours_single_open_per_day():
    json_ = {
        'monday': [
            {
                'type': 'open',
                'value': 32400
            },
            {
                'type': 'close',
                'value': 72000
            }
        ],
        'tuesday': [
            {
                'type': 'open',
                'value': 32400
            },
            {
                'type': 'close',
                'value': 72000
            }
        ],
        'wednesday': [
            {
                'type': 'open',
                'value': 32400
            },
            {
                'type': 'close',
                'value': 72000
            }
        ],
        'thursday': [
            {
                'type': 'open',
                'value': 32400
            },
            {
                'type': 'close',
                'value': 72000
            }
        ],
        'friday': [
            {
                'type': 'open',
                'value': 32400
            },
            {
                'type': 'close',
                'value': 72000
            }
        ],
        'saturday': [
            {
                'type': 'open',
                'value': 32400
            },
            {
                'type': 'close',
                'value': 72000
            }
        ],
        'sunday': [
            {
                'type': 'open',
                'value': 32400
            },
            {
                'type': 'close',
                'value': 72000
            }
        ],
    }

    expect = (
        'Monday: 9 AM - 8 PM'
        '\n'
        'Tuesday: 9 AM - 8 PM'
        '\n'
        'Wednesday: 9 AM - 8 PM'
        '\n'
        'Thursday: 9 AM - 8 PM'
        '\n'
        'Friday: 9 AM - 8 PM'
        '\n'
        'Saturday: 9 AM - 8 PM'
        '\n'
        'Sunday: 9 AM - 8 PM'
    )

    assert json_to_hours(json_) == expect


def test_json_to_hours_not_sorted():
    json_ = {
        'monday': [
            {
                'type': 'close',
                'value': 72000
            },
            {
                'type': 'open',
                'value': 32400
            },
        ],
    }

    expect = (
        'Monday: 9 AM - 8 PM'
        '\n'
        'Tuesday: Closed'
        '\n'
        'Wednesday: Closed'
        '\n'
        'Thursday: Closed'
        '\n'
        'Friday: Closed'
        '\n'
        'Saturday: Closed'
        '\n'
        'Sunday: Closed'
    )

    assert json_to_hours(json_) == expect


def test_json_to_hours_multi_per_day():
    json_ = {
        'tuesday': [
            {
                'type': 'open',
                'value': 10800
            },
            {
                'type': 'close',
                'value': 36000
            },
            {
                'type': 'open',
                'value': 39600
            },
            {
                'type': 'close',
                'value': 50400
            },
            {
                'type': 'open',
                'value': 61200
            },
            {
                'type': 'close',
                'value': 79200
            },
        ],
    }

    expect = (
        'Monday: Closed'
        '\n'
        'Tuesday: 3 AM - 10 AM, 11 AM - 2 PM, 5 PM - 10 PM'
        '\n'
        'Wednesday: Closed'
        '\n'
        'Thursday: Closed'
        '\n'
        'Friday: Closed'
        '\n'
        'Saturday: Closed'
        '\n'
        'Sunday: Closed'
    )

    assert json_to_hours(json_) == expect


def test_json_to_hours_cross_day():
    json_ = {
        'monday': [
            {
                'type': 'open',
                'value': 79200
            },
        ],
        'tuesday': [
            {
                'type': 'close',
                'value': 32400
            },
        ],
    }

    expect = (
        'Monday: 10 PM - 9 AM'
        '\n'
        'Tuesday: Closed'
        '\n'
        'Wednesday: Closed'
        '\n'
        'Thursday: Closed'
        '\n'
        'Friday: Closed'
        '\n'
        'Saturday: Closed'
        '\n'
        'Sunday: Closed'
    )

    assert json_to_hours(json_) == expect


def test_json_to_hours_cross_week():
    json_ = {
        'monday': [
            {
                'type': 'close',
                'value': 32400
            },
        ],
        'sunday': [
            {
                'type': 'open',
                'value': 79200
            },
        ],
    }

    expect = (
        'Monday: Closed'
        '\n'
        'Tuesday: Closed'
        '\n'
        'Wednesday: Closed'
        '\n'
        'Thursday: Closed'
        '\n'
        'Friday: Closed'
        '\n'
        'Saturday: Closed'
        '\n'
        'Sunday: 10 PM - 9 AM'
    )

    assert json_to_hours(json_) == expect


def test_json_to_hours_cross_many_days():
    json_ = {
        'monday': [
            {
                'type': 'open',
                'value': 79200
            },
        ],
        'wednesday': [
            {
                'type': 'close',
                'value': 32400
            },
        ],
    }

    expect = (
        'Monday: 10 PM - 12 PM'
        '\n'
        'Tuesday: 0 AM - 12 PM'
        '\n'
        'Wednesday: 0 AM - 9 AM'
        '\n'
        'Thursday: Closed'
        '\n'
        'Friday: Closed'
        '\n'
        'Saturday: Closed'
        '\n'
        'Sunday: Closed'
    )

    assert json_to_hours(json_) == expect


def test_json_to_hours_cross_week_many_days():
    json_ = {
        'tuesday': [
            {
                'type': 'close',
                'value': 32400
            },
        ],
        'sunday': [
            {
                'type': 'open',
                'value': 79200
            },
        ],
    }

    expect = (
        'Monday: 0 AM - 12 PM'
        '\n'
        'Tuesday: 0 AM - 9 AM'
        '\n'
        'Wednesday: Closed'
        '\n'
        'Thursday: Closed'
        '\n'
        'Friday: Closed'
        '\n'
        'Saturday: Closed'
        '\n'
        'Sunday: 10 PM - 12 PM'
    )

    assert json_to_hours(json_) == expect


def test_json_to_hours_not_opened():
    json_ = {
        'monday': [
            {
                'type': 'open',
                'value': 32400
            },
            {
                'type': 'close',
                'value': 72000
            }
        ],
        'tuesday': [],
        'sunday': [
            {
                'type': 'open',
                'value': 32400
            },
            {
                'type': 'close',
                'value': 72000
            }
        ],
    }

    expect = (
        'Monday: 9 AM - 8 PM'
        '\n'
        'Tuesday: Closed'
        '\n'
        'Wednesday: Closed'
        '\n'
        'Thursday: Closed'
        '\n'
        'Friday: Closed'
        '\n'
        'Saturday: Closed'
        '\n'
        'Sunday: 9 AM - 8 PM'
    )

    assert json_to_hours(json_) == expect


def test_json_to_hours_at_head_miss_opening():
    json_ = {
        'monday': [
            {
                'type': 'close',
                'value': 72000
            }
        ],
        'tuesday': [
            {
                'type': 'open',
                'value': 32400
            },
            {
                'type': 'close',
                'value': 72000
            }
        ],
    }

    with pytest.raises(MissOpeningHourError):
        json_to_hours(json_)


def test_json_to_hours_mid_week_miss_opening():
    json_ = {
        'monday': [
            {
                'type': 'open',
                'value': 32400
            },
            {
                'type': 'close',
                'value': 72000
            },
        ],
        'tuesday': [
            {
                'type': 'close',
                'value': 72000
            }
        ],
    }

    with pytest.raises(MissOpeningHourError):
        json_to_hours(json_)


def test_json_to_hours_mid_day_miss_opening():
    json_ = {
        'tuesday': [
            {
                'type': 'open',
                'value': 10800
            },
            {
                'type': 'close',
                'value': 36000
            },
            {
                'type': 'close',
                'value': 50400
            },
            {
                'type': 'open',
                'value': 61200
            },
            {
                'type': 'close',
                'value': 79200
            },
        ],
    }

    with pytest.raises(MissOpeningHourError):
        json_to_hours(json_)


def test_json_to_hours_cross_week_miss_opening():
    json_ = {
        'monday': [
            {
                'type': 'close',
                'value': 32400
            },
        ],
        'sunday': [
            {
                'type': 'close',
                'value': 79200
            },
        ],
    }

    with pytest.raises(MissOpeningHourError):
        json_to_hours(json_)


def test_json_to_hours_cross_many_days_missing_opening():
    json_ = {
        'monday': [
            {
                'type': 'close',
                'value': 79200
            },
        ],
        'wednesday': [
            {
                'type': 'close',
                'value': 32400
            },
        ],
    }

    with pytest.raises(MissOpeningHourError):
        json_to_hours(json_)


def test_json_to_hours_at_head_miss_closing():
    json_ = {
        'monday': [
            {
                'type': 'open',
                'value': 72000
            }
        ],
        'tuesday': [
            {
                'type': 'open',
                'value': 32400
            },
            {
                'type': 'close',
                'value': 72000
            }
        ],
    }

    with pytest.raises(MissClosingHourError):
        json_to_hours(json_)


def test_json_to_hours_mid_week_miss_closing():
    json_ = {
        'monday': [
            {
                'type': 'open',
                'value': 32400
            },
        ],
        'tuesday': [
            {
                'type': 'open',
                'value': 72000
            }
        ],
    }

    with pytest.raises(MissClosingHourError):
        json_to_hours(json_)


def test_json_to_hours_mid_day_miss_closing():
    json_ = {
        'tuesday': [
            {
                'type': 'open',
                'value': 10800
            },
            {
                'type': 'close',
                'value': 36000
            },
            {
                'type': 'open',
                'value': 50400
            },
            {
                'type': 'open',
                'value': 61200
            },
            {
                'type': 'close',
                'value': 79200
            },
        ],
    }

    with pytest.raises(MissClosingHourError):
        json_to_hours(json_)


def test_json_to_hours_cross_week_miss_closing():
    json_ = {
        'monday': [
            {
                'type': 'open',
                'value': 32400
            },
        ],
        'sunday': [
            {
                'type': 'open',
                'value': 79200
            },
        ],
    }

    with pytest.raises(MissClosingHourError):
        json_to_hours(json_)


def test_json_to_hours_cross_many_days_missing_closing():
    json_ = {
        'monday': [
            {
                'type': 'open',
                'value': 79200
            },
        ],
        'wednesday': [
            {
                'type': 'open',
                'value': 32400
            },
        ],
    }

    with pytest.raises(MissClosingHourError):
        json_to_hours(json_)


def test_json_to_hours_not_full_hour():
    json_ = {
        'monday': [
            {
                'type': 'open',
                'value': 79201
            },
        ],
        'wednesday': [
            {
                'type': 'close',
                'value': 32400
            },
        ],
    }

    with pytest.raises(NotFullHourError):
        json_to_hours(json_)


def test_json_to_hours_exceed_24_hours():
    json_ = {
        'monday': [
            {
                'type': 'open',
                'value': 79200
            },
            {
                'type': 'close',
                'value': 86400
            },
        ],
    }

    with pytest.raises(NextDayError):
        json_to_hours(json_)
